package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Represents complete key of the web-resource tag.
 * Complete key consists of plugin key and web-resource key and allows to obtain particular web-resource.
 *
 * @since v3.5.27
 */
@ExperimentalApi
public class CompleteWebResourceKey {
    private final String webResourceKey;
    private final String pluginKey;

    public CompleteWebResourceKey(String pluginKey, String webResourceKey) {
        this.pluginKey = pluginKey;
        this.webResourceKey = webResourceKey;
    }

    public String getWebResourceKey() {
        return webResourceKey;
    }

    public String getPluginKey() {
        return pluginKey;
    }

    public String getCompleteKey() {
        return pluginKey + ":" + webResourceKey;
    }

    @Override
    public String toString() {
        return "CompleteWebResourceKey{" + getCompleteKey() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompleteWebResourceKey that = (CompleteWebResourceKey) o;
        return webResourceKey.equals(that.webResourceKey) && pluginKey.equals(that.pluginKey);
    }

    @Override
    public int hashCode() {
        int result = webResourceKey.hashCode();
        result = 31 * result + pluginKey.hashCode();
        return result;
    }
}