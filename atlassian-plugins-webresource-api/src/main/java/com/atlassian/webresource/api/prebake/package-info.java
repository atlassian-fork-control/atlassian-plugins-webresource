/**
 * Provides the ability to pre-compute ("prebake") all possible resource urls
 * supported by this application, for a given set of requireContext/requireResource calls.
 * <p>
 * To provide this ability, conditions and transforms are required to implement
 * {@link com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition}
 * and
 * {@link com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory}
 * respectively. Such implementations are require to provide two capabilities:
 * </p>
 * <ul>
 * <li>{@code computeDimensions()}: indicate all possible query-params this implementations supports</li>
 * <li>{@code addToUrl(UrlBuilder urlBuilder, Coordinate coord)}: add to url for the given Coordinate.
 *     During pre-bake, is called instead of the existing {@code addToUrl(UrlBuilder urlBuilder)}.
 * </li>
 * </ul>
 */
package com.atlassian.webresource.api.prebake;