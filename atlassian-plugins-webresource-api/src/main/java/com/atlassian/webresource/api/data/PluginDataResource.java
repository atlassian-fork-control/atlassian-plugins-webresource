package com.atlassian.webresource.api.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.WebResource;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Container for a data key and value
 *
 * @since v3.0
 */
public interface PluginDataResource extends WebResource {
    String getKey();

    /**
     * Since 3.4.0, async data may normally or exceptionally complete, or timeout.
     * In the case of an exceptional completion or timeout, this method will throw NoSuchElementException.
     *
     * @throws NoSuchElementException in the case of an exceptional completion or timeout.
     * @deprecated use getData() instead.
     */
    Jsonable getJsonable();

    Optional<Jsonable> getData();

}
