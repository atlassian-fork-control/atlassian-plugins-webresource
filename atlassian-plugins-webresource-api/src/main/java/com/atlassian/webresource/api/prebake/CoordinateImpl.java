package com.atlassian.webresource.api.prebake;

import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 */
class CoordinateImpl implements Coordinate {
    private final Map<String, String> params;

    CoordinateImpl(List<QueryParam> c) {
        this.params = new LinkedHashMap<>();
        for (QueryParam keyval : c) {
            if (keyval.queryValue.isPresent()) {
                params.put(keyval.queryKey, keyval.queryValue.get());
            }
        }
    }

    @Override
    public void copyTo(UrlBuilder urlBuilder, String key) {
        if (params.containsKey(key)) {
            urlBuilder.addToQueryString(key, params.get(key));
        }
    }

    @Override
    public String get(String key) {
        return params.get(key);
    }

    @Override
    public Iterable<String> getKeys() {
        return params.keySet();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoordinateImpl that = (CoordinateImpl) o;

        return params.equals(that.params);

    }

    @Override
    public int hashCode() {
        return params.hashCode();
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "params=" + params +
                '}';
    }

}
