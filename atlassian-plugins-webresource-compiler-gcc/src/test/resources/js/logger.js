define(function () {
    function Logger(level) {

        if (!level) {
            level = "INFO";
        }

        var log = function(message) {
            console.log("[" + level + "] " + new Date() + " : " + message);
        }

        return log;
    }

    return Logger;
});