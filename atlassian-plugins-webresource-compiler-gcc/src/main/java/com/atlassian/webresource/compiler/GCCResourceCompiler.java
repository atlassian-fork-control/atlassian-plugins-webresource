package com.atlassian.webresource.compiler;

import com.atlassian.webresource.spi.CompilerEntry;
import com.atlassian.webresource.spi.ResourceCompiler;
import com.google.common.base.Preconditions;
import com.google.javascript.jscomp.AbstractCommandLineRunner;
import com.google.javascript.jscomp.CompilationLevel;
import com.google.javascript.jscomp.Compiler;
import com.google.javascript.jscomp.CompilerInput;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.SourceFile;
import com.google.javascript.rhino.InputId;
import com.google.javascript.rhino.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.webresource.compiler.GCCUtil.initOptionsFromLevel;

/**
 * Implementation based on Google Closure Compiler.
 *
 * @since v3.5.10
 */
public class GCCResourceCompiler implements ResourceCompiler {

    private static final Logger log = LoggerFactory.getLogger(GCCResourceCompiler.class);

    private final CompilerOptions options;
    private final Compiler compiler;
    private final List<SourceFile> externs;

    private GCCResourceCompiler(
            @Nonnull final PrintStream out,
            @Nonnull final CompilerOptions options) {
        this.options = Preconditions.checkNotNull(options);
        this.compiler = new Compiler(Preconditions.checkNotNull(out));

        // Create externs based on provided options
        try {
            this.externs = AbstractCommandLineRunner.getBuiltinExterns(options.getEnvironment());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public GCCResourceCompiler() {
        this(
                System.out,
                initOptionsFromLevel(CompilationLevel.WHITESPACE_ONLY)
        );
    }

    @Override
    public void compile(@Nonnull final Stream<CompilerEntry> entries) {
        List<SourceFile> sources = Preconditions.checkNotNull(entries, "entries can't be null")
                .flatMap(GCCUtil::entryToSource)
                .collect(Collectors.toList());
        if (log.isDebugEnabled()) {
            log.debug("compiling sources: {}", sources);
        }
        compiler.compile(externs, sources, options);
    }

    @Override
    public String content(@Nonnull final String key) {
        Node node = getNodeByKey(Preconditions.checkNotNull(key, "key can't be null"));
        if (node == null) {
            log.warn("can't find Node for: {}", key);
            return null;
        }
        return compiler.toSource(node);
    }

    private Node getNodeByKey(String key) {
        InputId id = new InputId(key);
        CompilerInput input = compiler.getInput(id);
        if (input == null) {
            log.warn("can't find CompilerInput for: {}", key);
            return null;
        }
        return input.getAstRoot(compiler);
    }

}
