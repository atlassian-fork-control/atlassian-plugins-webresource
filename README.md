# Atlassian Webresources

## Docs

[Web Resource Plugin Module](https://developer.atlassian.com/docs/getting-started/plugin-modules/web-resource-plugin-module)
[Web Resource Transformer Plugin Module](https://developer.atlassian.com/docs/getting-started/plugin-modules/web-resource-transformer-plugin-module)

## Description

A framework for Atlassian plugins to manage Javascript and CSS resources provided by plugins. The
framework allows for plugins to declare web resources, bind them to contexts in applications, and
set up dependencies so that requiring one resource will also pull in all its dependencies.

Batching of related resources into a single download is also provided.

## JavaScript unit testing

The `atlassian-plugins-webresource-plugin` submodule uses [karma](http://karma-runner.github.io/) for javascript unit testing.
This requires [npm](https://www.npmjs.org/).

The maven build shells out to invoke `npm install` and `npm test` to run the tests. You must have npm installed to do this.

To run tests in watch mode while developing:

- ensure you have invoked: `npm install`,
- then run karma: `npm run test/watch`

## Testing CT-CDN features with JIRA

The script `scripts/jira-ctcdn-debug` contains a collection of tools that can be used to test CT-CDN and prebaking 
features with JIRA. The script makes uses of `jmake` to manage the lifecycle of a named JIRA instance, thus allowing 
quick restarts after the `mappings.json` file is modified. Docker is employed to run a web-server/proxy combination 
that serves prebaked resources.

### Environment variables:

- **JIRA_SRC**: JIRA source folder. Defaults to '~/src/jira'.
- **JIRA_PORT**: JIRA port. Defaults to '8090'.
- **CTCDN_HOST**: Address of the dockerized ct-cdn. Defaults to 'localhost', should be set to the appropriate 
docker-machine ip address when running on OS X (docker-machine ip default).
- **CTCDN_PORT**: Port of the dockerized ct-cdn. Defaults to '58090'.
- **CTCDN_ROOT**: Folder used to store prebaked resources and mappings. Defaults to '~/.ct-cdn'.

### Commands:

- **start**: starts a JIRA instance with CT-CDN support and prebaking functionalities enabled. The instance is  
automatically configured to fetch prebaked resources from $CTCDN_HOST:$CTCDN_PORT, and will be run in background to 
allow subsequent invocations of the `jira-ctcdn-debug` script to be issued from the same shell.
- **stop**: stops the JIRA instance.
- **restart**: quick restart of the JIRA instance, equivalent to stopping and restarting Tomcat without recompiling 
the application. This command is usually issued to reload changes in the `mappings.json` file.
- **start-cdn**: Starts a dockerized web-server (with proxy) on port $CTCDN_PORT to serve prebaked resources from the 
$CTCDN_ROOT folder.
- **prebake**: Triggers the `prebake` operation on the JIRA instance, waits for its completion, downloads the 
prebaked resources, and installs them in the $CTCDN_ROOT folder.
- **clearmap**: Removes all mappings and prebaked resources. This command is usually issued to prepare an instance 
before prebaking.

### Usage examples:

Start a new instance with an empty `mappings.json` file:

./jira-ctcdn-debug clearmap start


Prebake and reload mappings (a running instance is required):

./jira-ctcdn-debug prebake restart


Starts the docker containers required to serve the prebaked resources

./jira-ctcdn-debug start-cdn


## Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

## Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR).

## Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUGWEB).

## Documentation

- [JIRA Web Resources](https://developer.atlassian.com/display/JIRADEV/Web+Resource+Plugin+Module)
- [Confluence Web Resources](https://developer.atlassian.com/display/CONFDEV/Web+Resource+Module)
- [Async loading](https://extranet.atlassian.com/display/FED/Async+Web+Resource+Loading+is+now+bundled+in+webresources+3.0)

## Architecture

```
|---------------------------------------|  |---------------------------------------|
| Layer 4: Java and JS API              |  | Layer 4: URL Generation and Resource  |
|                                       |  | Serving                               |
|                                       |  |                                       |
| WebResourceAssembler, WebResourceSet, |  | RequestState, Router, Controller.     |
| WRM.require                           |  |                                       |
|---------------------------------------|  |---------------------------------------|

|----------------------------------------------------------------------------------|
| Layer 3: Dependency resolving and batching algorithms.                           |
|                                                                                  |
| Helpers, UrlGenerationHelpers, ResourceServingHelpers.                           |
|----------------------------------------------------------------------------------|

|----------------------------------------------------------------------------------|
| Layer 2: The state of the system, information about the web resources and config |
|                                                                                  |
| Config, Snapshot, Globals.                                                       |
|----------------------------------------------------------------------------------|

|---------------------------------------|  |---------------------------------------|
| Layer 1: SPI                          |  | Layer 1: Platform and Plugin System   |
|                                       |  |                                       |
| WebResourceIntegration,               |  |                                       |
| ResourceBatchingConfiguration         |  |                                       |
|---------------------------------------|  |---------------------------------------|
```
