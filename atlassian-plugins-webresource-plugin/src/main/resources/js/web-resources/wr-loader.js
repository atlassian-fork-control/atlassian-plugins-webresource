// wr! loader, needed to require Web Resource from AMD module, returns the `window` as the result.
define("wr", {
    load: function (name, req, onload, config) {
        // requirejs would complain if we use `window` here, so getting it using a trick.
        function getWindow() {
            return this;
        }
        onload(getWindow());
    }
});