// AMD module loader for WRM.
;(function () {
    // Protecting from loading it twice.
    if (WRM.atlassianModule) {
        return;
    }

    // Atlassian module helper, creates special version of `define` with overridden dependencies.
    WRM.atlassianModule = function atlassianModule(name, resolvedDependencies, cb) {
        function atlassianModuleDefine() {
            // Parsing arguments.
            var unresolvedDependencies;
            var factoryFn = arguments[arguments.length - 1];
            if (!isFunction(factoryFn)) {
                throw new Error("no factory function!")
            }
            if (arguments.length == 1) {
                unresolvedDependencies = [];
            } else if (arguments.length == 2) {
                if (!isArray(arguments[0])) {
                    throw new Error("dependencies must be an array!")
                }
                unresolvedDependencies = arguments[0];
            } else {
                throw new Error("wrong arguments for define!")
            }

            // Resolving dependency names.
            var dependencies = [];
            for (var i = 0; i < unresolvedDependencies.length; i++) {
                var dependency = unresolvedDependencies[i];
                dependencies.push(resolvedDependencies[dependency] || dependency);
            }

            return define(name, dependencies, factoryFn);
        }
        atlassianModuleDefine.amd = define.amd;
        cb(atlassianModuleDefine)
    };

    // # Support.
    function isFunction(o) {
        return toString.call(o) === '[object Function]';
    }
    function isArray(o) {
        return toString.call(o) === '[object Array]';
    }
}());
