// less! loader, needed to require LESS from AMD module, returns the null as the result.
define("less", {
    load: function (name, req, onload, config) {
        onload(null);
    }
});