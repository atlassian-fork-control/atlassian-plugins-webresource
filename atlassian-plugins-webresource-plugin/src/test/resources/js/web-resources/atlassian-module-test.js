module("atlassian-module", {
    setup: function() {
        this._previousDefine = window.define;
        var calls = [];
        this.calls = calls;
        window.define = function(name, dependencies, factoryFn) {
            calls.push(Array.prototype.slice.call(arguments));
        }
    },
    teardown: function() {
        window.define = this._previousDefine;
    }
});

test("should define module with given name and resolved dependencies", function() {
    WRM.atlassianModule('a', {'./b': 'b'}, function(define) {
        define(['./b', 'c'], function (b1) {return "module a"});
    });

    var args = this.calls[0];
    deepEqual("a", args[0]);
    deepEqual(['b', 'c'], args[1]);
    deepEqual('module a', args[2]())
});