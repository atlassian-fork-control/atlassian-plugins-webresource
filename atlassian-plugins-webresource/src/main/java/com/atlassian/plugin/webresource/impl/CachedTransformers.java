package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Wrapper around transformer to cache it.
 *
 * @since v3.3.2
 */
public class CachedTransformers
{
    private final List<WebResourceTransformation> transformations;
    private Set<String> paramKeys;

    public CachedTransformers(List<WebResourceTransformation> transformations)
    {
        this.transformations = transformations;
    }


    /**
     * Add transformer parameters to url. If error wil be thrown it will be intercepted and ignored.
     * @return if there are legacy transformers.
     */
    public boolean addToUrlSafely(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy, String type, TransformerCache transformerCache, TransformerParameters transformerParameters, String webResourceKey)
    {
        boolean containsLegacyTransformers = false;
        for (WebResourceTransformation transformation : transformations)
        {
            if (transformation.matches(type))
            {
                try
                {
                    transformation.addTransformParameters(transformerCache, transformerParameters, urlBuilder, urlBuildingStrategy);
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("error thrown in transformer during url generation for " + webResourceKey, e);
                }
                if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache))
                {
                    containsLegacyTransformers = true;
                }
            }
        }
        return containsLegacyTransformers;
    }

    public List<WebResourceTransformation> getTransformations()
    {
        return transformations;
    }

    public Dimensions computeDimensions(TransformerCache transformerCache)
    {
        Dimensions d = Dimensions.empty();
        for (WebResourceTransformation transformation : transformations)
        {
            d = d.product(transformation.computeDimensions(transformerCache));
        }
        return d;
    }

    public void computeParamKeys(TransformerCache transformerCache)
    {
        if (paramKeys != null) {
            throw new IllegalStateException("invalid usage, method computeParamKeys should be called only once!");
        }
        paramKeys = new HashSet<>();
        for (WebResourceTransformation transformation : transformations)
        {
            transformation.computeDimensions(transformerCache).cartesianProduct().forEach(coordinate -> {
                for (String key : coordinate.getKeys()) {
                    paramKeys.add(key);
                }
            });
        }
    }

    /**
     * Get list of param keys used by this transformers.
     */
    public Set<String> getParamKeys() {
        return paramKeys;
    }
}
