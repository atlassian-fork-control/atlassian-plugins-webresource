package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.List;
import java.util.Map;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public abstract class ResourceUrl {

    private final List<PrebakeError> prebakeErrors;

    public ResourceUrl(List<PrebakeError> prebakeErrors) {
        this.prebakeErrors = prebakeErrors;
    }

    public abstract String getName();

    public abstract String getKey();

    public abstract String getType();

    public abstract String getUrl(boolean isAbsolute);

    public abstract Map<String, String> getParams();

    public boolean isTainted() {
        return !prebakeErrors.isEmpty();
    }

    public List<PrebakeError> getPrebakeErrors() {
        return prebakeErrors;
    }

    public abstract PluginUrlResource.BatchType getBatchType();

    public abstract List<Resource> getResources(RequestCache requestCache);
}
