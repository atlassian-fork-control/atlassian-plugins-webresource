package com.atlassian.plugin.webresource.impl.annotators;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Wraps script inside of async loader.
 *
 * @since v3.5.11
 */
public class AsyncLoaderAnnotator extends ResourceContentAnnotator {

    @Override
    public void afterAllResourcesInBatch(LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream) throws IOException {
        after(url, params, stream);
    }

    @Override
    public int beforeAllResourcesInBatch(LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream) throws IOException {
        return before(url, params, stream);
    }

    @Override
    public int beforeResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        return before(url, params, stream);
    }

    @Override
    public void afterResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        after(url, params, stream);
    }

    protected void after(String url, final Map<String, String> params, OutputStream stream) throws IOException {
        if (null != params.get(Config.ASYNC_SCRIPT_PARAM_NAME)) {
            stream.write("\n});".getBytes());
        }
    }

    protected int before(String url, final Map<String, String> params, OutputStream stream) throws IOException {
        if (null != params.get(Config.ASYNC_SCRIPT_PARAM_NAME)) {
            stream.write(("WRM.InOrderLoader.define(function () {\n").getBytes());
            return 1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}