package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.Set;

/**
 * Applies a fixed set of transformers to all resources by type.
 * These transformers are always applied after pluggable transformers.
 *
 * @since v3.1.0
 */
public interface StaticTransformers {
    // TODO refactor it into the interface of the transformer.
    //
    // It's a temporary way to get list of parameters used by static transformers, it should be refactored
    // into proper transformer interface.
    String[] PARAMETERS_USED = new String[]{RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY};

    /**
     * Returns the {@link Dimensions} of all registered transformers.
     *
     * @return dimensions of all registeres transformers
     */
    Dimensions computeDimensions();

    /**
     * @param bundle for which you want to get dimensions.
     * @return {@link Dimensions} related to given bundle.
     * @since 3.5.22
     */
    Dimensions computeBundleDimensions(Bundle bundle);

    /**
     * Contributes to the URL for all static transformers matching the given type
     *
     * @param locationType          type of resource (eg js, css, less)
     * @param transformerParameters parameters to pass to transformers
     * @param urlBuilder            url building to contribute to
     */
    void addToUrl(String locationType, TransformerParameters transformerParameters, UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy);

    /**
     * Performs transformation for all static transformers for the given type.
     *
     * @param content               content to transform
     * @param transformerParameters the {@link TransformerParameters} object
     * @param resourceLocation      resource location
     * @param filePath              file path
     * @param queryParams           url querystring containing information for transformers
     * @param sourceUrl             url of original source file
     * @return a DownloadableResource representing the completed transformation
     */
    Content transform(Content content, TransformerParameters transformerParameters, ResourceLocation resourceLocation, String filePath,
                      QueryParams queryParams, String sourceUrl);

    /**
     * Get list of param keys used by this transformers.
     *
     * @since 3.5.27
     */
    public Set<String> getParamKeys();
}
