package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.List;

/**
 * Implementation of PluginUrlResource
 * @since 3.0
 */
abstract class DefaultPluginUrlResource<T extends PluginUrlResourceParams> implements PluginUrlResource<T>
{

    protected final ResourceUrl resourceUrl;

    public DefaultPluginUrlResource(ResourceUrl resourceUrl)
    {
        this.resourceUrl = resourceUrl;
    }

    @Override
    public String getStaticUrl(UrlMode urlMode)
    {
        return resourceUrl.getUrl(urlMode == UrlMode.ABSOLUTE);
    }

    @Override
    public boolean isTainted()
    {
        return resourceUrl.isTainted();
    }

    @Override
    public List<PrebakeError> getPrebakeErrors() {
        return resourceUrl.getPrebakeErrors();
    }

    @Override
    public String toString()
    {
        return resourceUrl.getKey() + ":" + resourceUrl.getName();
    }

    @Override
    public String getKey() {
        return resourceUrl.getKey();
    }

    @Override
    public BatchType getBatchType() {
        return resourceUrl.getBatchType();
    }
}
