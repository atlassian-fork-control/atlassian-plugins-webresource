package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.List;

/**
 * Builds superbatch resources
 * @since v3.0
 */
public class SuperBatchBuilder
{
    private final ResourceDependencyResolver dependencyResolver;
    private final PluginResourceLocator pluginResourceLocator;

    public SuperBatchBuilder(ResourceDependencyResolver dependencyResolver, PluginResourceLocator pluginResourceLocator)
    {
        this.dependencyResolver = dependencyResolver;
        this.pluginResourceLocator = pluginResourceLocator;
    }

    /**
     * Get all super-batch resources that match the given filter.
     */
    public Iterable<PluginResource> build()
    {
        final Iterable<ModuleDescriptorStub> superBatchModuleKeys = dependencyResolver.getSuperBatchDependencies();
        final List<PluginResource> resources = new ArrayList<>();

        SuperBatchPluginResource superBatchPluginResource = new SuperBatchPluginResource();
        boolean isEmpty = true;
        for (final ModuleDescriptorStub moduleDescriptor : superBatchModuleKeys)
        {
            for (final PluginResource pluginResource : pluginResourceLocator.getPluginResources(moduleDescriptor.getCompleteKey()))
            {
                // record the content of each super batch to enable front-end dependency tracking
                if (pluginResource instanceof BatchPluginResource)
                {
                    superBatchPluginResource.addBatchedWebResourceDescriptor(((BatchPluginResource) pluginResource).getModuleCompleteKey());
                    isEmpty &= false;
                }
            }
        }

        // here we sort the order of the batches. The unconditional batches should come first and the most conditional batches should come last.
        if (!isEmpty)
        {
            resources.add(superBatchPluginResource);
        }

        return resources;
    }
}
