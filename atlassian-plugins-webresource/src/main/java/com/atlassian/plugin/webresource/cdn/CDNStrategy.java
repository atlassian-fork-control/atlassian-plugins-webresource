package com.atlassian.plugin.webresource.cdn;

import com.atlassian.plugin.webresource.prebake.PrebakeConfig;

import java.util.Optional;

/**
 * Interface for CDN strategies. CDN strategies take a URL and transform it to another URL.
 *
 * @since v3.0.5
 */
public interface CDNStrategy {
    /**
     * @return true if a CDN is supported
     */
    boolean supportsCdn();

    /**
     * Transforms the given URL to be served from the CDN. The given URL is assumed to be relative to the current host.
     * @param url relative URL to transformed
     * @return the URL to a CDN version of the given URL.
     */
    String transformRelativeUrl(String url);

    /**
     * Define how pre-baked resources in CT-CDN will be consumed by transforming local URLs according to a mapping file.
     * It'll be red the first time {@link com.atlassian.plugin.webresource.WebResourceIntegration#isCtCdnMappingEnabled()} is "true" and cached "forever".
     * Runtime changes to the configuration will be ignored.
     * @return Configuration for pre-baked CDN.
     * @since v3.5.0 (moved to WebResourceIntegration as of v3.5.16)
     *
     */
    default Optional<PrebakeConfig> getPrebakeConfig() {
        return Optional.empty();
    }
}
