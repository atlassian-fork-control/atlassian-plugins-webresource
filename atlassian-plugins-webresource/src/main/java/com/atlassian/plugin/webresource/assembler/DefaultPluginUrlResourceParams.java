package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.BooleanUtils;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @since v3.0
 */
abstract class DefaultPluginUrlResourceParams implements PluginUrlResourceParams {
    protected final Map<String, String> params;

    public DefaultPluginUrlResourceParams(Map<String, String> params, String key, BatchType batchType) {
        this.params = new LinkedHashMap<>(params);
        this.params.put(Config.WRM_KEY_PARAM_NAME, key);
        this.params.put(Config.WRM_BATCH_TYPE_PARAM_NAME, batchType.name().toLowerCase());
        if (!ieOnly()) {
            this.params.remove(Config.IEONLY_PARAM_NAME);
        }
    }

    @Override
    @Deprecated
    public String conditionalComment() {
        return params.get(Config.CONDITIONAL_COMMENT_PARAM_NAME);
    }

    @Override
    @Deprecated
    public boolean ieOnly() {
        return BooleanUtils.toBoolean(params.get(Config.IEONLY_PARAM_NAME));
    }

    @Override
    public Map<String, String> other() {
        return Maps.filterEntries(params, new Predicate<Map.Entry<String, String>>() {
            @Override
            public boolean apply(@Nullable Map.Entry<String, String> input) {
                String key = input.getKey();
                return !key.equals(Config.CONDITIONAL_COMMENT_PARAM_NAME)
                        && !key.equals(Config.IEONLY_PARAM_NAME);
            }
        });
    }

    @Override
    public Map<String, String> all() {
        return Collections.unmodifiableMap(params);
    }
}
