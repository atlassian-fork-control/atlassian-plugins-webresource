package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.config.Config;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class JavascriptWebResource extends AbstractWebResourceFormatter {
    // It should behave exactly the same with or without AMD being enabled, but, just to be sure we didn't break the
    // stuff in BTF, the new AMD behaviour will be enabled only if AMD is enabled.
    // This check should be removed in the Cloud-only version of WRM.
    private final boolean isAmdEnabled;

    /**
     * Keeping the backward compatible constructor because JIRA uses this class. It shouldn't use it, it's a private
     * implementation and not a part of the API. This constructor will be removed the next Cloud-only version of WRM.
     */
    @Deprecated
    public JavascriptWebResource() {
        this(false);
    }

    public JavascriptWebResource(boolean isAmdEnabled) {
        this.isAmdEnabled = isAmdEnabled;
    }

    private static final List<String> HANDLED_PARAMETERS = asList("charset",
            Config.WRM_KEY_PARAM_NAME, Config.WRM_BATCH_TYPE_PARAM_NAME, Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME,
            Config.ASYNC_SCRIPT_PARAM_NAME);

    public boolean matches(String name) {
        return name != null && (name.endsWith(Config.JS_EXTENSION) || (isAmdEnabled && name.endsWith(Config.SOY_EXTENSION)));
    }

    public String formatResource(String url, Map<String, String> attributes) {
        return formatResource(url, attributes, false);
    }

    public String formatResource(String url, Map<String, String> attributes, boolean enableDeferJsAttribute) {
        if (!isValid(attributes)) {
            return "";
        }
        StringBuilder buffer = new StringBuilder("<script type=\"text/javascript\" ");
        buffer.append("src=\"").append(StringEscapeUtils.escapeHtml4(url)).append("\" ");

        if (enableDeferJsAttribute && !attributes.containsKey(Config.ASYNC_SCRIPT_PARAM_NAME)) {
            buffer.append("defer ");
        }

        buffer.append(StringUtils.join(getParametersAsAttributes(attributes).iterator(), " "));
        buffer.append("></script>\n");

        return buffer.toString();
    }

    protected List<String> getAttributeParameters() {
        return HANDLED_PARAMETERS;
    }
}
