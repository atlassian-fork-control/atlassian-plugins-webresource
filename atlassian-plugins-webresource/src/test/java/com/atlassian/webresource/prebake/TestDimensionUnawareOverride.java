package com.atlassian.webresource.prebake;

import com.atlassian.plugin.webresource.prebake.DimensionUnawareOverride;
import com.atlassian.webresource.api.prebake.Dimensions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static com.atlassian.plugin.webresource.prebake.DimensionUnawareOverride.DimensionOverride;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestDimensionUnawareOverride {

    private static final Collection<String> booleanDimensionsValues = asList("true", null);
    private static final Collection<String> localeDimensionValues = asList("en-US", "en-GB");

    @Test
    public void testDimensionUnawareOverride() {
        Collection<DimensionOverride> overrides = new ArrayList<>();
        overrides.add(new DimensionOverride("aClassName", "aKey", booleanDimensionsValues));
        overrides.add(new DimensionOverride("bClassName", "bKey", booleanDimensionsValues));
        overrides.add(new DimensionOverride("cClassName", "cKey", localeDimensionValues));
        DimensionUnawareOverride.setup(overrides);

        assertThat(DimensionUnawareOverride.size(), equalTo(3));

        assertTrue(DimensionUnawareOverride.contains("aClassName"));
        assertThat(DimensionUnawareOverride.key("aClassName"), equalTo("aKey"));
        Dimensions aDimensions = Dimensions.empty().andExactly("aKey", booleanDimensionsValues);
        assertThat(DimensionUnawareOverride.dimensions("aClassName"), equalTo(aDimensions));

        assertTrue(DimensionUnawareOverride.contains("bClassName"));
        assertThat(DimensionUnawareOverride.key("bClassName"), equalTo("bKey"));
        Dimensions bDimensions = Dimensions.empty().andExactly("bKey", booleanDimensionsValues);
        assertThat(DimensionUnawareOverride.dimensions("bClassName"), equalTo(bDimensions));

        assertTrue(DimensionUnawareOverride.contains("cClassName"));
        assertThat(DimensionUnawareOverride.key("cClassName"), equalTo("cKey"));
        Dimensions cDimensions = Dimensions.empty().andExactly("cKey", localeDimensionValues);
        assertThat(DimensionUnawareOverride.dimensions("cClassName"), equalTo(cDimensions));

        DimensionUnawareOverride.reset();
        assertThat(DimensionUnawareOverride.size(), equalTo(0));
        assertFalse(DimensionUnawareOverride.contains("aClassName"));
        assertFalse(DimensionUnawareOverride.contains("bClassName"));
        assertFalse(DimensionUnawareOverride.contains("cClassName"));
    }

}
