package com.atlassian.plugin.webresource.prebake;

import com.atlassian.plugin.webresource.cdn.mapper.MappingParserException;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestPrebakeConfig {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testReplacement() throws IOException, MappingParserException {
        File f = PrebakeConfig.forPattern("/path/to/${state}/mappings").getMappingLocation("$TAT&");
        assertThat(f, equalTo(new File("/path/to/$TAT&/mappings")));
    }

    @Test
    public void testNoReplacement() throws IOException, MappingParserException {
        File f = PrebakeConfig.forPattern("/path/to/noState/mappings").getMappingLocation("$TAT&");
        assertThat(f, equalTo(new File("/path/to/noState/mappings")));
    }

    @Test
    public void testWrongReplacement() throws IOException, MappingParserException {
        File f = PrebakeConfig.forPattern("/path/to/${stateNonExist}/mappings").getMappingLocation("$TAT&");
        assertThat(f, equalTo(new File("/path/to/${stateNonExist}/mappings")));
    }

    @Test
    public void testWrongSyntax() throws IOException, MappingParserException {
        File f = PrebakeConfig.forPattern("/path/to/${stateError/mappings").getMappingLocation("$TAT&");
        assertThat(f, equalTo(new File("/path/to/${stateError/mappings")));
    }

}
