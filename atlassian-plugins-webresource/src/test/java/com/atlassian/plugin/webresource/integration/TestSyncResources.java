package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestSyncResources extends TestCase {
    private final boolean useAsync;

    @Parameterized.Parameters(name = "useAsync is {0}")
    public static Collection<Object[]> data() {
        return asList(new Object[][]
                {
                        {true},
                        {false}
                });
    }

    public TestSyncResources(boolean useAsync) {
        this.useAsync = useAsync;
    }

    @Test
    public void shouldWriteSyncResources() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(singletonList(
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .plugin("com.atlassian.plugins.atlassian-plugins-webresource-plugin")
                .webResource("in-order-loader")
                .resource("in-order-loader.js")
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .resource("b1.js")
                .webResource("system")
                .resource("almond.js")
                .end();

        wr.requireResource("plugin:a");
        final String firstDrainHtml = wr.pathsAsHtml();
        assertThat(firstDrainHtml, checkForInOrderLoaderPresence());
        assertThat(firstDrainHtml, matches(checkForInOrderLoader(
                new String[]{
                        "content of almond.js"
                },
                new String[]{
                        webResourceBatchUrl("plugin:a", "js")
                }
        )));

        wr.requireResource("plugin:b");
        final String secondDrainHtml = wr.pathsAsHtml();
        assertThat(secondDrainHtml, not(matches(
                "content of almond.js"
        )));
        assertThat(secondDrainHtml, matches(
                webResourceBatchUrl("plugin:b", "js")
        ));
    }

    @Test
    public void shouldWriteSyncResourcesWithDependencies() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(singletonList(
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .plugin("com.atlassian.plugins.atlassian-plugins-webresource-plugin")
                .webResource("in-order-loader")
                .resource("in-order-loader.js")
                .plugin("pre")
                .webResource("pre")
                .resource("pre.js")
                .plugin("metrics")
                .webResource("analytics")
                .dependency("pre:pre")
                .resource("analytics.js")
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .resource("b1.js")
                .webResource("system")
                .dependency("metrics:analytics")
                .resource("almond.js")
                .end();

        wr.requireResource("plugin:a");
        final String firstDrainHtml = wr.pathsAsHtml();
        assertThat(firstDrainHtml, checkForInOrderLoaderPresence());
        assertThat(firstDrainHtml, matches(checkForInOrderLoader(
                new String[]{
                        "content of pre.js",
                        "content of analytics.js",
                        "content of almond.js"},
                new String[]{
                        webResourceBatchUrl("plugin:a", "js")
                }
        )));

        wr.requireResource("plugin:b");
        final String secondDrainHtml = wr.pathsAsHtml();
        assertThat(secondDrainHtml, not(matches(
                "content of almond.js"
        )));
        assertThat(secondDrainHtml, matches(
                webResourceBatchUrl("plugin:b", "js")
        ));
    }

    @Test
    public void shouldMinifySyncResources() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(singletonList(
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .plugin("plugin")
                .webResource("system")
                .resource("almond.js")
                .file("almond.min.js", "content of almond.min.js")
                .end();

        final String firstDrainHtml = wr.pathsAsHtml();
        assertThat(firstDrainHtml, matches(
                "content of almond.min.js"
        ));
    }

    @Test
    public void shouldWorkWithNonExistingSyncResources() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(asList(
                        new CompleteWebResourceKey("plugin-which-does-not-exists", "web-resource-key"),
                        new CompleteWebResourceKey("plugin", "incorrect-key"),
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .plugin("plugin")
                .webResource("b")
                .resource("b1.js")
                .webResource("system")
                .resource("almond.js")
                .resource("style.css")
                .end();

        wr.requireResource("plugin:b");

        final String firstDrainHtml = wr.pathsAsHtml();
        assertThat(firstDrainHtml, matches(
                "content of almond.js",
                webResourceBatchUrl("plugin:b", "js", obtainParams())
        ));
    }

    @Test
    public void shouldExcludeSyncResourcesFromSuperbatch() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(singletonList(
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .addToSuperbatch("plugin:a", "plugin:system")
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .resource("b1.js")
                .webResource("system")
                .resource("almond.js")
                .end();

        wr.requireResource("plugin:b");

        String html = wr.pathsAsHtml();
        assertThat(html, matches(
                "content of almond.js",
                getSuperbatchURI(),
                webResourceBatchUrl("plugin:b", "js", obtainParams())
        ));

        String superbatchContent = wr.getContent(getSuperbatchURI());
        assertThat(superbatchContent, matches(
                "content of a1.js"
        ));
        assertThat(superbatchContent, not(matches(
                "content of almond.js"
        )));
    }

    private Map<String, String> obtainParams() {
        return (useAsync) ? buildMap("async", "true") : new HashMap<>();
    }

    @Test
    public void shouldExcludeSyncResourcesWithDependenciesFromSuperbatch() {
        wr.configure()
                .useAsync(useAsync)
                .markSyncResources(singletonList(
                        new CompleteWebResourceKey("plugin", "system")
                ))
                .addToSuperbatch("plugin:a", "plugin:system", "pre:pre")
                .plugin("com.atlassian.plugins.atlassian-plugins-webresource-plugin")
                .webResource("in-order-loader")
                .resource("in-order-loader.js")
                .plugin("pre")
                .webResource("pre")
                .resource("pre.js")
                .plugin("metrics")
                .webResource("analytics")
                .dependency("pre:pre")
                .resource("analytics.js")
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .resource("b1.js")
                .webResource("system")
                .dependency("metrics:analytics")
                .resource("almond.js")
                .end();

        wr.requireResource("plugin:b");

        String firstDrainHtml = wr.pathsAsHtml();
        assertThat(firstDrainHtml, checkForInOrderLoaderPresence());
        assertThat(firstDrainHtml, matches(checkForInOrderLoader(
                new String[]{
                        "content of pre.js",
                        "content of analytics.js",
                        "content of almond.js"},
                new String[]{
                        getSuperbatchURI(),
                        webResourceBatchUrl("plugin:b", "js", obtainParams())})
        ));

        String superbatchContent = wr.getContent(getSuperbatchURI());
        assertThat(superbatchContent, matches(
                "content of a1.js"
        ));
        assertThat(superbatchContent, not(matches(
                "content of almond.js",
                "content of pre.js"
        )));
    }

    private String getSuperbatchURI() {
        return contextBatchUrl(encodeSuperbatchKey(), "js");
    }

    private String encodeSuperbatchKey() {
        return Config.SUPER_BATCH_CONTEXT_KEY + ",-" + Config.SYNCBATCH_CONTEXT_KEY;
    }

    private Matcher<Object> checkForInOrderLoaderPresence() {
        return (useAsync) ? shouldInOrderLoaderBePresent() : shouldInOrderLoaderBeAbsent();
    }

    private Matcher<Object> shouldInOrderLoaderBePresent() {
        return matches("content of in-order-loader.js");
    }

    private Matcher<Object> shouldInOrderLoaderBeAbsent() {
        return not(shouldInOrderLoaderBePresent());
    }

    private String[] checkForInOrderLoader(String... regexps) {
        if (useAsync) {
            String[] newRegexps = new String[regexps.length + 1];
            System.arraycopy(regexps, 0, newRegexps, 1, regexps.length);
            newRegexps[0] = "content of in-order-loader.js";
            return newRegexps;
        }
        return regexps;
    }

    private String[] checkForInOrderLoader(String[] syncContent, String[] asyncContent) {
        if (useAsync) {
            String[] newRegexps = new String[syncContent.length + asyncContent.length + 1];
            System.arraycopy(syncContent, 0, newRegexps, 0, syncContent.length);
            newRegexps[syncContent.length] = "content of in-order-loader.js";
            System.arraycopy(asyncContent, 0, newRegexps, syncContent.length + 1, asyncContent.length);
            return newRegexps;
        } else {
            String[] newRegexps = new String[syncContent.length + asyncContent.length];
            System.arraycopy(syncContent, 0, newRegexps, 0, syncContent.length);
            System.arraycopy(asyncContent, 0, newRegexps, syncContent.length, asyncContent.length);
            return newRegexps;
        }
    }
}