package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

public class AddLocationDimensionAware implements DimensionAwareWebResourceTransformerFactory {

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty().andExactly("location", "true", "false");
    }

    public DimensionAwareTransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new DimensionAwareTransformerUrlBuilder() {
            @Override
            public void addToUrl(UrlBuilder urlBuilder) {
                urlBuilder.addToQueryString("location", "true");
            }

            @Override
            public void addToUrl(UrlBuilder urlBuilder, Coordinate coord) {
                urlBuilder.addToQueryString("location", coord.get("location"));
            }
        };
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
        return (resource, params) -> new CharSequenceDownloadableResource(resource.nextResource()) {
            protected CharSequence transform(CharSequence originalContent) {
                if ("true".equals(params.get("location"))) {
                    if (parameters.isAmdModule()) {
                        return "location=" + parameters.getPluginKey() + "/" +
                                resource.location().getLocation() + " (transformer)\n" + originalContent;
                    } else {
                        return "location=" + parameters.getPluginKey() + ":" + parameters.getModuleKey() + "/" +
                                resource.location().getLocation() + " (transformer)\n" + originalContent;
                    }
                } else {
                    return originalContent;
                }
            }
        };
    }
}