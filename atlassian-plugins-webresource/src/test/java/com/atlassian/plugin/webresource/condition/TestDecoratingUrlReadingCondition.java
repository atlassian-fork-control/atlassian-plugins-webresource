package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestDecoratingUrlReadingCondition {
    @Test
    public void testCanEncodeStateIntoUrl() {
        DecoratingCondition decoratingCondition = decorate(mock(Condition.class));

        assertFalse(decoratingCondition.canEncodeStateIntoUrl());
    }

    @Test
    public void testShouldDisplayImmediate() {
        UrlBuildingStrategy urlBuilderStrategy = UrlBuildingStrategy.normal();
        Condition condition = mock(Condition.class);
        Map<String, Object> context = emptyMap();

        DecoratingCondition decoratingCondition = decorate(condition);
        decoratingCondition.shouldDisplayImmediate(context, urlBuilderStrategy);

        verify(condition).shouldDisplay(context);
    }
}
