package com.atlassian.plugin.webresource.util;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class UrlCdnMatcher extends BaseMatcher {
    @Override
    public void describeTo(final Description description) {
        description.appendText("has CDN");
    }

    @Override
    public boolean matches(Object actual) {
        return (actual != null) && actual.toString().contains("-CDN/");
    }

    public static UrlCdnMatcher hasCdn() {
        return new UrlCdnMatcher();
    }
}