/* eslint-env node */
var path = require('path');
var JQUERY_VERSION = process.env.JQUERY_VERSION;

var JQUERY_MAP = {
    '1.4':  function() { return require.resolve('jquery-1.4/jquery.js') },
    '1.5':  function() { return require.resolve('jquery-1.5/jquery.js') },
    '1.7':  function() { return require.resolve('jquery-1.7/jquery.js') },
    '1.8':  function() { return require.resolve('jquery-1.8/jquery.js') },
    '1.9':  function() { return require.resolve('jquery-1.9') },
    '1.11': function() { return require.resolve('jquery-1.11') },
    '2.2':  function() { return require.resolve('jquery-2.2') },
    '3.3':  function() { return require.resolve('jquery-3.3') },
    'legacy': function() { return JQUERY_MAP['1.8']() },
};

module.exports = function(config) {
    var jqueryDependency;
    if (JQUERY_MAP.hasOwnProperty(JQUERY_VERSION)) {
        jqueryDependency = JQUERY_MAP[JQUERY_VERSION]();
    } else {
        jqueryDependency = require.resolve('jquery');
        JQUERY_VERSION = 'default';
    }
    // eslint-disable-next-line
    console.log({jquery: JQUERY_VERSION});

    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: 'src',

        // frameworks to use
        frameworks: ['qunit', 'sinon'],

        plugins: [
            'karma-qunit',
            'karma-sinon',
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher'
        ],

        // list of files / patterns to load in the browser
        files: [
            require.resolve('sinon-qunit'),
            jqueryDependency,
            // deps
            'test/resources/js/wrm/fixture/ajs-mock.js',
            'test/resources/js/wrm/fixture/wrm-mock.js',
            'test/resources/js/wrm/fixture/wrm-data-mock.js',
            // curl
            'main/resources/js/wrm/curl-config.js',
            'main/resources/js/wrm/lib/curl-0.7.3.js',
            'main/resources/js/wrm/lib/curl-css-0.7.3.js',
            'main/resources/js/wrm/lib/curl-js-0.7.3.js',
            // wrm
            'main/resources/js/wrm/logger.js',
            'main/resources/js/wrm/jquery.js',
            'main/resources/js/wrm/underscore.js',
            'main/resources/js/wrm/builder.js',
            'main/resources/js/wrm/context-path.js',
            'main/resources/js/wrm/require-handler.js',
            'main/resources/js/wrm/index.js',
            // amd
            require.resolve('almond/almond.js'),
            '../target/modules-test-build.js',
            // tests
            'test/resources/js/**/*-test.js'
        ],


        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: path.resolve(
                __dirname,
                'target/surefire-reports/',
                'karma-results-' + JQUERY_VERSION + '.xml'
            ),
            classNameFormatter: function(browser) {
                var parts = [];
                parts.push(browser.name);
                parts.push('WRM REST jquery ' + JQUERY_VERSION);
                return parts.map(function(part) {
                    return part.replace(/[\s.]/g, '_');
                }).join('.');
            },
            suite: 'WRM_REST'
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        //browsers: ['Chrome', 'Safari', 'Firefox', 'Opera', 'IE11 - Win7', 'IE10 - Win7', 'IE9 - Win7'],
        browsers: ['PhantomJS'],


        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};
