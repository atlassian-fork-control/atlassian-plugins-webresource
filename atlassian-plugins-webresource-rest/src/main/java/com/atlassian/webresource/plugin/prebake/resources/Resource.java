package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.List;

/**
 * An generic class for wrapping url-accessible resources.
 * <p>
 * <p> The additional {@link Resource#equals(Object)} and {@link Resource#hashCode()} methods are employed during the
 * prebake process to ensure that each resource is collected only once.
 *
 * @since 3.5.13
 */
public abstract class Resource {

    /**
     * Returns a URL that can be used to fetch the resource.
     *
     * @return a URL that can be used to fetch the resource.
     */
    public abstract String getUrl();

    /**
     * True if the resource is not safe for prebaking (i.e., tainted), false otherwise. Resources can get tainted if
     * they are processed by dimension-unaware transformers and conditions, or if they are associated with
     * transformers and conditions that are deemed inherently unsafe for prebaking.
     *
     * @return true if the resource is not safe for prebaking (i.e., tainted), false otherwise.
     */
    public abstract boolean isTainted();

    /**
     * Returns the list of {@link PrebakeError}s that were generated while prebaking the resource.
     *
     * @return list of {@link PrebakeError}s that were generated while prebaking the resource.
     */
    public abstract List<PrebakeError> getPrebakeErrors();

    /**
     * Returns a name that identifies the resource
     *
     * @return a name that identifies the resource
     */
    public abstract String getName();

    /**
     * Returns the extension associated with the resource.
     *
     * @return extension associated with the resource.
     */
    public abstract String getExtension();

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Resource)) {
            return false;
        }

        String url1 = getUrl();
        String url2 = ((Resource) o).getUrl();
        return url1.equals(url2);
    }

    @Override
    public int hashCode() {
        return getUrl().hashCode();
    }

}
