package com.atlassian.webresource.plugin.async;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.data.PluginDataResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.plugin.webresource.Flags.isDevMode;

/**
 * Implementation of AsyncWebResourceLoader
 *
 * @since v3.0
 */
public class AsyncWebResourceLoaderImpl implements AsyncWebResourceLoader {
    private static final Logger logger = LoggerFactory.getLogger(AsyncWebResourceLoaderImpl.class);

    private final WebResourceAssemblerFactory webResourceAssemblerFactory;

    public AsyncWebResourceLoaderImpl(WebResourceAssemblerFactory webResourceAssemblerFactory) {
        this.webResourceAssemblerFactory = webResourceAssemblerFactory;
    }

    @Override
    public ResourcesAndData resolve(Set<String> webResources, Set<String> contexts, Set<String> excludeResources,
                                    Set<String> excludeContexts) throws IOException {

        final Collection<Resource> resources = new ArrayList<>();
        final Map<String, String> data = new LinkedHashMap<>();
        final Map<String, String> errors = new LinkedHashMap<>();

        // This current impl loops until we get all async data, and returns it as one blob.
        // Ideally we should stream, see PLUGWEB-254
        WebResourceSet webResourceSet;
        do {
            webResourceSet = resolveWebResourceSet(webResources, contexts, excludeResources, excludeContexts);
            for (WebResource webResource : webResourceSet.getResources()) {
                if (webResource instanceof PluginDataResource) {
                    PluginDataResource dataResource = (PluginDataResource) webResource;
                    Optional<Jsonable> jsonable = dataResource.getData();
                    if (jsonable.isPresent()) {
                        data.put(dataResource.getKey(), jsonToString(dataResource.getJsonable()));
                    } else {
                        errors.put(dataResource.getKey(), "\"\"");
                    }
                } else if (webResource instanceof PluginJsResource) {
                    PluginJsResource jsResource = (PluginJsResource) webResource;
                    resources.add(new Resource(jsResource.getStaticUrl(UrlMode.RELATIVE), ResourceType.JAVASCRIPT,
                            jsResource.getParams().conditionalComment(), jsResource.getParams().ieOnly(), null,
                            jsResource.getKey(), jsResource.getBatchType()));
                } else if (webResource instanceof PluginCssResource) {
                    PluginCssResource cssResource = (PluginCssResource) webResource;
                    checkLogMediaQueryError(cssResource);
                    resources.add(new Resource(cssResource.getStaticUrl(UrlMode.RELATIVE), ResourceType.CSS,
                            cssResource.getParams().conditionalComment(), cssResource.getParams().ieOnly(),
                            cssResource.getParams().media(), cssResource.getKey(), cssResource.getBatchType()));
                }
            }
        } while (!webResourceSet.isComplete());

        return new ResourcesAndData(resources, data, errors);
    }

    private static String jsonToString(Jsonable jsonable) throws IOException {
        StringWriter out = new StringWriter();
        jsonable.write(out);
        return out.toString();
    }

    /**
     * Checks if the given resource contains a media query, and logs a warning if it does.
     * Logging is done in dev mode only.
     *
     * @param cssResource css resource to check
     */
    private void checkLogMediaQueryError(PluginCssResource cssResource) {
        String media = cssResource.getParams().media();
        if (media != null && !media.isEmpty() && !"all".equals(media)
                && isDevMode()) {
            logger.warn("WARN: asynchronously loading a CSS resource containing a media query: {}",
                    cssResource.getStaticUrl(UrlMode.RELATIVE));
        }
    }

    private WebResourceSet resolveWebResourceSet(Set<String> webResources, Set<String> contexts,
                                                 Set<String> excludeResources, Set<String> excludeContexts) {
        if (logger.isDebugEnabled()) {
            logger.debug("Requiring webresources {}", webResources);
            logger.debug("Requiring contexts {}", contexts);
            logger.debug("Excluding webresource {}", excludeResources);
            logger.debug("Excluding contexts {}", excludeContexts);
        }

        // Async requests should not include the super batch by default - unless explicitly required
        WebResourceAssembler assembler = webResourceAssemblerFactory.create().includeSuperbatchResources(false).build();
        assembler.resources().exclude(excludeResources, excludeContexts);

        for (String webResource : webResources) {
            if (Config.isWebResourceKey(webResource)) {
                assembler.resources().requireWebResource(webResource);
            } else {
                assembler.resources().requireModule(webResource);
            }
        }

        for (String context : contexts)
            assembler.resources().requireContext(context);

        return assembler.assembled().pollIncludedResources();
    }
}
