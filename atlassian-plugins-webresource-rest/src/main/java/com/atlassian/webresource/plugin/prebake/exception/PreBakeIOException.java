package com.atlassian.webresource.plugin.prebake.exception;

/**
 * Occurs when IO error happened during pre-bake operation.
 *
 * @since v3.5.0
 */
public class PreBakeIOException extends PreBakeException {
    public PreBakeIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreBakeIOException(String message) {
        super(message);
    }
}
