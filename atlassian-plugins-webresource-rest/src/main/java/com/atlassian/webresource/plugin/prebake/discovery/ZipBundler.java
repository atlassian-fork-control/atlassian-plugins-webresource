package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.cdn.mapper.DefaultMapping;
import com.atlassian.plugin.webresource.cdn.mapper.DefaultMappingSet;
import com.atlassian.plugin.webresource.cdn.mapper.Mapping;
import com.atlassian.plugin.webresource.cdn.mapper.MappingParser;
import com.atlassian.plugin.webresource.cdn.mapper.MappingSet;
import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;
import com.atlassian.webresource.plugin.prebake.util.FileUtil;
import com.atlassian.webresource.plugin.prebake.util.PreBakeUtil;
import com.atlassian.webresource.plugin.prebake.util.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * A {@link Bundler} implementation that stores prebaked {@link Resource}s in the file system.
 *
 * <p> In addition to the methods inherited from {@link Bundler}, this class provides a
 * {@link ZipBundler#createBundleZip()} that can be used to create a {@code bundle.zip} file containing the results
 * of the prebaking operation.
 *
 * <p> It is important to note that the following state-altering methods will throw an {@link IllegalStateException}
 * if they are invoked after creating the {@code bundle.zip} through {@link ZipBundler#createBundleZip()}:
 *
 * <ul>
 *     <li>{@link ZipBundler#addResource(String, Resource, ResourceContent)}</li>
 *     <li>{@link ZipBundler#addTaintedResource(String, Resource)}</li>
 *     <li>{@link ZipBundler#addTaintedResource(String, String, PrebakeError...)}</li>
 * </ul>
 *
 * @since v3.5.29
 */
public final class ZipBundler implements Bundler {

    private static final Logger log = LoggerFactory.getLogger(ZipBundler.class);
    private static final DecimalFormat PERCENT_FORMAT = new DecimalFormat("#0.00");
    private static final MappingParser mappingParser = new MappingParser();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private volatile boolean zipCreated = false;

    private final String globalStateHash;
    private final Path outputFolder;
    private final Path bundleDir;
    private final Path bundleZip;
    private final Path tempResourcesDir;
    private final boolean checkHashCollisions;

    private final Map<String, String> mappings = new HashMap<>();
    private final Map<String, TaintedResource> tainted = new HashMap<>();

    /**
     * Creates a new {@link ZipBundler} without hash collision checking
     *
     * @param globalStateHash state of the instance. Will be used to identify the {@code bundle.zip}.
     * @param outputFolder folder used by the {@link ZipBundler} to store intermediate results and the
     *                     {@code bundle.zip} file.
     */
    public ZipBundler(String globalStateHash, Path outputFolder) {
        this(globalStateHash, outputFolder, false);
    }

    /**
     * Creates a new {@link ZipBundler}.
     *
     * @param globalStateHash state of the instance. Will be used to identify the {@code bundle.zip}.
     * @param outputFolder folder used by the {@link ZipBundler} to store intermediate results and the
     *                     {@code bundle.zip} file.
     * @param checkHashCollisions if true, enforces hash collision checking. The hash collision algorithm checks the
     *                            contents of files with the same hash, and taints the clashing resource. It's a
     *                            relatively disk-intensive operation, and might introduce slowdowns in case there
     *                            are many false positives (i.e., resources with the same content associated to
     *                            different URLs).
     */
    public ZipBundler(String globalStateHash, Path outputFolder, boolean checkHashCollisions) {
        this.globalStateHash = globalStateHash;
        this.outputFolder = outputFolder;
        bundleDir = outputFolder.resolve(PreBakeUtil.BUNDLE_ZIP_DIR);
        bundleZip = outputFolder.resolve(PreBakeUtil.RELATIVE_ZIP);
        tempResourcesDir = bundleDir.resolve(PreBakeUtil.RELATIVE_RESOURCES);
        boolean created = tempResourcesDir.toFile().mkdirs();
        if (!created) {
            String msg = String.format("Could not create temporary resource folder '%s'", tempResourcesDir.toString());
            throw new PreBakeIOException(msg);
        }
        this.checkHashCollisions = checkHashCollisions;
    }

    /**
     * Returns the {@code globalStateHash} that will be associated with the {@code bundle.zip} file.
     */
    public String getGlobalStateHash() {
        return globalStateHash;
    }

    /**
     * Returns the {@link Path} of the {@code bundle.zip} file. This file will only be available after the method
     * {@link ZipBundler#createBundleZip()} has been called.
     */
    public Path getBundleZipPath() {
        return bundleZip;
    }

    @Override
    public Optional<String> addResource(String url, Resource resource, ResourceContent content) {
        if (zipCreated) {
            throw new IllegalStateException("Cannot add more resources after bundle.zip creation");
        }
        if (resource.isTainted()) {
            throw new IllegalArgumentException("Cannot add tainted resource '" + url + "'");
        }

        lock.writeLock().lock();
        try {
            // Return immediately if we're trying to save the same url
            if (mappings.containsKey(url)) {
                return Optional.of(mappings.get(url));
            }
            return saveResource(url, resource, content);
        } finally {
            lock.writeLock().unlock();
        }
    }

    private Optional<String> saveResource(String url, Resource resource, ResourceContent content) {
        byte[] bytes = content.getContent();
        String hashedFilename = PreBakeUtil.hash(bytes) + resource.getExtension();
        String hashedUrl = "/" + hashedFilename;
        Path target = tempResourcesDir.resolve(hashedFilename);

        try {
            if (!Files.exists(target)) {
                // File doesn't exist: save contents
                saveContent(target, bytes);
                mappings.put(url, hashedUrl);
                return Optional.of(hashedUrl);
            } else if (checkHashCollisions && contentIsDifferent(target, bytes)) {
                // Same hash, different content: collision
                String collidedWith = getUrlWithHashedFilename(hashedFilename);
                String msg = String.format("Hash collision between '%s' and '%s'", url, collidedWith);
                log.error(msg);
                addTaintedResource(url, resource.getName(), PrebakeErrorFactory.from(msg));
                return Optional.empty();
            } else {
                // File exists, content is the same (or not checking hash collisions): simply return the hashed filename
                mappings.put(url, hashedUrl);
                return Optional.of(hashedFilename);
            }
        } catch (IOException e) {
            String msg = "Cannot save resource content to " + target.toAbsolutePath();
            log.error(msg, e);
            addTaintedResource(url, resource.getName(), PrebakeErrorFactory.from(msg, e));
            return Optional.empty();
        }
    }

    private void saveContent(Path target, byte[] bytes) throws IOException {
        InputStream is = new ByteArrayInputStream(bytes);
        Files.copy(is, target);
        is.close();
    }

    private boolean contentIsDifferent(Path target, byte[] bytes) throws IOException {
        try {
            byte[] existing = Files.readAllBytes(target);
            return !Arrays.equals(bytes, existing);
        } catch (IOException e) {
            throw new IOException("Error comparing resource content while checking hash collision", e);
        }
    }

    private String getUrlWithHashedFilename(String hashedFilename) {
        // Calling directly get() without checking if the Option is empty or not.
        // It's fine to do this, since by the time we're invoking this function we will get at least one result from
        // this selection.
        return mappings.entrySet().stream()
                .filter(e -> e.getValue().equals(hashedFilename))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();
    }

    @Override
    public void addTaintedResource(String url, Resource resource) {
        if (zipCreated) {
            throw new IllegalStateException("Cannot add more tainted resources after bundle.zip creation");
        }

        if (!resource.isTainted()) {
            return;
        }

        lock.writeLock().lock();
        try {
            // Return immediately if we're trying to save the same url
            if (tainted.containsKey(url)) {
                return;
            }
            TaintedResource tr = new TaintedResource(
                    resource.getUrl(),
                    resource.getName(),
                    resource.getPrebakeErrors()
            );
            tainted.put(url, tr);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void addTaintedResource(String url, String name, PrebakeError... errors) {
        if (zipCreated) {
            throw new IllegalStateException("Cannot add more tainted resources after bundle.zip creation");
        }

        lock.writeLock().lock();
        try {
            // Return immediately if we're trying to save the same url
            if (tainted.containsKey(url)) {
                return;
            }
            TaintedResource tr = new TaintedResource(url, name, asList(errors));
            tainted.put(url, tr);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean isMapped(String url) {
        lock.readLock().lock();
        try {
            return mappings.containsKey(url);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public boolean isTainted(String url) {
        lock.readLock().lock();
        try {
            return tainted.containsKey(url);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public int getMappedCount() {
        lock.readLock().lock();
        try {
            return mappings.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public Optional<String> getMapping(String url) {
        lock.readLock().lock();
        try {
            return Optional.ofNullable(mappings.get(url));
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public int getTaintedCount() {
        lock.readLock().lock();
        try {
            return tainted.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Creates a {@code bundle.zip} file with the following contents:
     *
     * <ul>
     *     <li>{@code mappings.json}: CT-CDN mappings</li>
     *     <li>{@code state.txt}: {@code globalstatehash} of the instance that was used to generate the resources</li>
     *     <li>{@code report.txt}: diagnostic information on the prebake process (tainted resources, coverage)</li>
     *     <li>{@code resources/}: folder containing the prebaked resources</li>
     * </ul>
     *
     * @throws PreBakeIOException If the {@code bundle.zip} file could not be created.
     */
    public void createBundleZip() throws PreBakeIOException {
        lock.writeLock().lock();
        try {
            if (zipCreated) {
                log.info("Skipping, bundle.zip has already been created.");
                return;
            }
            zipCreated = true;
            StopWatch stopwatch = new StopWatch();
            doCreateBundleZip();
            log.info("bundle.zip creation took " + stopwatch.getElapsedSecondsAndReset() + "s");
        } finally {
            lock.writeLock().unlock();
        }
    }

    private void doCreateBundleZip() throws PreBakeIOException {
        Collection<String> sources = new ArrayList<>();
        try {
            // Saving global state hash
            File globalStateHashFile = bundleDir.resolve(PreBakeUtil.RELATIVE_STATE).toFile();
            FileUtil.write2File(
                    globalStateHash.getBytes(PreBakeUtil.UTF8),
                    globalStateHashFile
            );
            sources.add(globalStateHashFile.getAbsolutePath());

            // Saving mappings
            File mappingsFile = bundleDir.resolve(PreBakeUtil.RELATIVE_MAPPINGS).toFile();
            FileUtil.write2File(
                    getMappingsAsByteArray(),
                    mappingsFile
            );
            sources.add(mappingsFile.getAbsolutePath());

            // Saving prebake report
            File prebakeReportFile = bundleDir.resolve(PreBakeUtil.RELATIVE_REPORT).toFile();
            writeBatchReport(prebakeReportFile);
            sources.add(prebakeReportFile.getAbsolutePath());

            sources.addAll(getPrebakedFileSet());
            FileUtil.zipFiles(outputFolder.toFile(), sources, bundleZip.toFile());
        } catch (IOException e) {
            String msg = String.format("Cannot create '%s'", bundleZip.toString());
            log.error(msg, e);
            throw new PreBakeIOException(msg, e);
        }
    }

    private byte[] getMappingsAsByteArray() throws IOException {
        Set<Mapping> ms = new HashSet<>();
        for (Map.Entry<String, String> e : mappings.entrySet()) {
            Mapping m = new DefaultMapping(
                    e.getKey(),
                    singletonList(e.getValue()).stream()
            );
            ms.add(m);
        }
        MappingSet mappingSet = new DefaultMappingSet(ms);
        return mappingParser.getAsString(mappingSet).getBytes(PreBakeUtil.UTF8);
    }

    private Set<String> getPrebakedFileSet() {
        return StreamSupport.stream(mappings.values().spliterator(), false)
                .map(url -> url.substring(1)) // Stripping away the first slash
                .map(tempResourcesDir::resolve) // Appending relative file name to base url
                .map(Path::toString) // Converting to String
                .sorted()
                .collect(Collectors.toSet());
    }

    private void writeBatchReport(File reportFile) throws IOException {
        FileWriter fw = new FileWriter(reportFile);
        BufferedWriter bw = new BufferedWriter(fw);

        int mapCount = mappings.size();
        int taintCount = tainted.size();
        int total = mapCount + taintCount;
        double coverage = total == 0 ? 0 : ((double) mapCount / total) * 100;

        bw.append("Coverage: ").append(PERCENT_FORMAT.format(coverage)).append("%\n");
        bw.append("Baked resources: ").append(String.valueOf(mapCount)).append("\n");
        bw.append("Tainted resources: ").append(String.valueOf(taintCount)).append("\n");
        for (TaintedResource tr : tainted.values()) {
            bw.append("\nRESOURCE: ");
            bw.append(tr.getUrl());
            bw.append("\n");
            List<PrebakeError> pes = tr.getPrebakeErrors();
            for (int i = 0; i < pes.size(); i++) {
                PrebakeError pe = pes.get(i);
                bw.append(String.valueOf(i)).append(") ");
                bw.append(pe.toString());
                bw.append("\n");
            }
        }

        bw.close();
        fw.close();
    }

}
