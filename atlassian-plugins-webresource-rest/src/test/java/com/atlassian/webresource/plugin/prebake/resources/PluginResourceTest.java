package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.assembler.DefaultPluginJsResource;
import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PluginResourceTest {

    private static final String ABSOLUTE_URL = "https://extranet.atlassian.com/jira/s/7783b989799b50d14fc0ac77b6f8b1a1-CDN/en_US-q95qxz/71003/b6b48b2829824b869586ac216d119363/2.3.1/_/download/batch/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib?locale=en-US";
    private static final String RELATIVE_URL = "/s/7783b989799b50d14fc0ac77b6f8b1a1-CDN/en_US-q95qxz/71003/b6b48b2829824b869586ac216d119363/2.3.1/_/download/batch/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib?locale=en-US";

    @Test
    public void testCSSResource() {
        String name = "resource_name";
        PluginCssResource css = mock(PluginCssResource.class);
        when(css.getStaticUrl(Mockito.any())).thenReturn(ABSOLUTE_URL);
        when(css.getPrebakeErrors()).thenReturn(emptyList());
        when(css.toString()).thenReturn(name);

        PluginResource pr = new PluginResource(css);
        assertThat(pr.getUrl(), equalTo(RELATIVE_URL));
        assertFalse(pr.isTainted());
        assertThat(pr.getPrebakeErrors().size(), equalTo(0));
        assertThat(pr.getName(), equalTo(name));
        assertThat(pr.getExtension(), equalTo(".css"));
    }

    @Test
    public void testJSResource() {
        String name = "resource_name";
        PluginJsResource js = mock(PluginJsResource.class);
        when(js.getStaticUrl(Mockito.any())).thenReturn(ABSOLUTE_URL);
        when(js.getPrebakeErrors()).thenReturn(emptyList());
        when(js.toString()).thenReturn(name);

        PluginResource pr = new PluginResource(js);
        assertThat(pr.getUrl(), equalTo(RELATIVE_URL));
        assertFalse(pr.isTainted());
        assertThat(pr.getPrebakeErrors().size(), equalTo(0));
        assertThat(pr.getName(), equalTo(name));
        assertThat(pr.getExtension(), equalTo(".js"));
    }

    @Test
    public void testTaintedResource() {
        List<PrebakeError> prebakeErrors = asList(
                PrebakeErrorFactory.from("test1"),
                PrebakeErrorFactory.from("test2")
        );

        ResourceUrl url = mock(ResourceUrl.class);
        when(url.isTainted()).thenReturn(true);
        when(url.getPrebakeErrors()).thenReturn(prebakeErrors);

        PluginResource pr = new PluginResource(new DefaultPluginJsResource(url));
        assertTrue(pr.isTainted());
        assertThat(pr.getPrebakeErrors(), equalTo(prebakeErrors));
    }

}
