package com.atlassian.webresource.plugin.rest;

import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.mockito.Mockito.verify;

public class TestListOfResources {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AsyncWebResourceLoader asyncWebResourceLoader;

    @Test
    public void getDecodesCommaSeparatedValues() throws Exception {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("wr1,wr2", "c1,c2", "xr1,xr2", "xc1,xc2");
        verify(asyncWebResourceLoader).resolve(Sets.newSet("wr1", "wr2"), Sets.newSet("c1", "c2"),
                Sets.newSet("xr1", "xr2"), Sets.newSet("xc1", "xc2"));
    }

    @Test
    public void getDecodesSingleValues() throws Exception {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("wr1", "c1", "xr1", "xc1");
        verify(asyncWebResourceLoader).resolve(singleton("wr1"), singleton("c1"), singleton("xr1"), singleton("xc1"));
    }

    @Test
    public void getDecodesEmptyValues() throws Exception {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("", "", "", "");
        verify(asyncWebResourceLoader).resolve(emptySet(), emptySet(), emptySet(), emptySet());
    }

    @Test
    public void postPassesRequestArgumentsThroughToAsyncWebResourceLoader() throws Exception {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.post(new ListOfResources.Request(
                asList("wr1", "wr2"),
                asList("c1", "c2"),
                asList("xr1", "xr2"),
                asList("xc1", "xc2")));
        verify(asyncWebResourceLoader).resolve(Sets.newSet("wr1", "wr2"), Sets.newSet("c1", "c2"),
                Sets.newSet("xr1", "xr2"), Sets.newSet("xc1", "xc2"));
    }

    @Test
    public void requestCanBeInstantiatedWithoutArgumentsAsRequiredByJersey() {
        new ListOfResources.Request();
    }
}
